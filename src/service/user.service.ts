import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/model/user';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public baseUrl = 'https://bob-backspring-app-staging.herokuapp.com/api/authenticate/users';
  public baseUrl1 ="https://bob-backspring-app-staging.herokuapp.com/api/authenticate/login";
  public host:string="https://bob-backspring-app-staging.herokuapp.com/api/authenticate"

  islogin = false;
  admin = false;
  suser = false;
  username: string; 
  password: string;
  listData : User[];
  public dataForm: FormGroup; 

  constructor(private http:HttpClient) { }
  /**méthode get ressources */
  public getRessource(url){
    return this.http.get(this.host+url);
  }

  /**getUserbyEmail */
login(username, password): Observable<Object>{
 return this.http.post(`${this.baseUrl1}`, {"username":username, "motDepass":password});
}

/**getUser by id  */
getData(id: number): Observable<Object> {
  return this.http.get(`${this.baseUrl}/${id}`);
}
/**saveUser */
createData(info: Object): Observable<Object> {
  return this.http.post(`${this.baseUrl}`, info);
}
/**UpdateUser */
updatedata(id: number, value: any): Observable<Object> {
  return this.http.put(`${this.baseUrl}/${id}`, value);
}
/**deleteUser */
deleteData(id: number): Observable<any> {
   
  return this.http.delete(`${this.baseUrl}/${id}`);
}
/**getAllUSer */
getAll(): Observable<any> {
   
    return this.http.get(`${this.baseUrl}`);
  }
}
