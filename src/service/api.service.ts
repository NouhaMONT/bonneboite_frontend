import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Entreprise } from 'src/app/model/entreprise';
import { Statistique } from 'src/app/model/statistique';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // public host:string="http://localhost:8889"
  public host:string="https://bob-backnode-app-staging.herokuapp.com"

  constructor(private http:HttpClient) { }

  public getRessource(url){
    return this.http.get(this.host+url);

  }
  public rechercheEntreprisesMV(matched_rome_label :string, city: string): Observable<Entreprise>{
     return this.http.get<Entreprise>(this.host+"/companiesMetierVille/"+matched_rome_label+"/"+city);
  }

  public rechercheByMetier(rome_code :string): Observable<Statistique>{
    return this.http.get<Statistique>(this.host+"/companiesMetier/"+rome_code);
 }

}
