import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Preference } from 'src/app/model/prefrence';

@Injectable({
  providedIn: 'root'
})
export class PreferenceService {
  public baseUrl = 'https://bob-backspring-app-staging.herokuapp.com/api/authenticate/prefrences';
  listData: Preference []; 
  public dataForm:  FormGroup;
   //ville_metier: string; 
    code_rome: string; 
    longitude: number; 
    latitude: number;
    

  constructor( private http: HttpClient) { }

/**get prefbyid */

getData(id: number): Observable<Object> {
  return this.http.get(`${this.baseUrl}/${id}`);
}

/**save preference */
createData(info: Object  ): Observable<Object> {
  console.log("testinfo"+info);
  return this.http.post(`${this.baseUrl}`, info);
  
}
/**delete pref */
deleteData(id: number): Observable<any> {
   
  return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
}
/**getAll pref */
getAll(): Observable<any> {
   
  return this.http.get(`${this.baseUrl}`);
}

}
