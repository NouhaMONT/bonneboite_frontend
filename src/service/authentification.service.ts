import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { User } from 'src/app/model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  public baseUrl1 ="https://bob-backspring-app-staging.herokuapp.com/api/authenticate/login";

  
  islogin = false;
  admin = false;
  suser = false;
  listData : User[];
  public dataForm: FormGroup; 
  public isAuthenticated:boolean; 
  public userAuthenticated; 
  public token;
  email:string; 
  user: any={};
  password: string;
  errorMessage: string;
  name: string;
  firstname: string;
  role: string; 
  nom: string; 
  t: string;

  constructor(private http:HttpClient,
    private router: Router, public toastr: ToastrService) { }

  /**getUserbyEmail */
  login(username, password): Observable<Object>{
    return this.http.post(`${this.baseUrl1}`, {username, password});
   }

  //  public onLogin(){
  //    this.login(this.email)
  //    .subscribe(response=>{
  //       this.user =response; 
  //       console.log(this.user );
  //       if (this.user.motDepass == this.password)
  //       {
  //         this.name= this.user.nom;
  //         this.token= {name: this.user.nom, role: this.user.role};
  //         this.islogin= true; 
  //         if (this.user)
  //       {
  //         this.isAuthenticated=true;
  //         this.userAuthenticated=this.user;
  //         this.saveAuthentificatedUser();
  //         this.router.navigateByUrl('/');
  //       }
  //       else{
  //         this.isAuthenticated=false;
  //         this.userAuthenticated= undefined;
  //       }
  //       }
  //       else
  //       {
  //         this.toastr.warning( 'Mot de Passe  Incorrecte ')
  //       }
  //     },
  //     error=>
  //     this.toastr.warning( 'Login Incorrecte ')
  //     );
  //  }

   public isAdmin(){
    if (this.user){
      if (this.user.role.indexOf('ADMIN')>-1)
      return true;
    }
    return false;
  }

  // public saveAuthentificatedUser(){
  //   if(this.userAuthenticated){
  //     localStorage.setItem('authToken', btoa(JSON.stringify(this.token)));
  //   }
  // }

  public loadAuthenticatedUserFromLStg(){
    
   let username= localStorage.getItem("name");
   let role=localStorage.getItem("role");
   let nom=localStorage.getItem("nom");
   let prenom=localStorage.getItem("prenom");
   let email=localStorage.getItem("email");
   let token=localStorage.getItem("token")
      
      let user ={name:username, role:role, nom:nom, firstname:prenom, email:email, t:token};
      
      console.log(user);
      this.userAuthenticated=user;
      console.log(this.userAuthenticated);
      this.isAuthenticated=true; 
      
    
  }

     isAuth(){
    if (this.isAuthenticated=true){
      return true;
    }
    return false;
    }

     public removeTokenLS(){
    //   localStorage.removeItem('authToken');
     localStorage.removeItem("name");
     localStorage.removeItem("role");
     localStorage.removeItem("nom");
     localStorage.removeItem("prenom");
     localStorage.removeItem("email");
     localStorage.removeItem("token")
       this.isAuthenticated=false;
    
      this.userAuthenticated=undefined;
    }

}
