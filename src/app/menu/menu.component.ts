import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/service/authentification.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  
  constructor(public authService : AuthentificationService,
    
    public router: Router) { 
      console.log("testmenu const")
    }
public name ='';
public prenom ='';
public role ='';
  ngOnInit(): void {
     this.authService.loadAuthenticatedUserFromLStg();
     console.log("testmenu avant ngOninit")

     this.name= localStorage.getItem('name');
     console.log("testmenu après ngOninit");
     this.role=localStorage.getItem('role'); 
     this.prenom=localStorage.getItem("prenom");

  }

  isAdmin(){
    if (this.role == "ADMIN"){
      return true;
    }
    return false;
  }

  isUser(){
    if (this.role == "USER"){
      return true;
    }
    return false;
   }

  isUserAuth(){
    this.authService.isAuth();
  }
  onLogout(){
    this.authService.removeTokenLS();
    // localStorage.removeItem('name');
    //localStorage.removeItem('authToken');
    //localStorage.removeItem('token');
    this.router.navigateByUrl('/login');
  }

}
