import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/service/user.service';


@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {
public users; 

  constructor(private userService: UserService, 
    public toastr: ToastrService, private router: Router, 
    public fb: FormBuilder) {
      console.log("test dans le const");
     }

  ngOnInit(): void {
    this.getUsers();

  }

  private getUsers(){
    this.userService.getRessource("/users")
    .subscribe(data=>{
      this.users = data; 
      console.log("test"+this.users);
    },err=>{
      console.log(err);
    })
  }
   
  removeData(id: number) {
    let conf= window.confirm('Etes vous sûr ?');
    if (conf)
     {
    this.userService.deleteData(id)
      .subscribe(
        data => {
          this.getUsers();
          this.toastr.warning(' Utilisateur supprimé !'); 
           this.router.navigateByUrl('/list-user');
        },
        error => {
          console.log(error)
        });
        
  }
}

infoUser(id : number){
    this.router.navigate(['/infoUser', id]);
}
// infoForm(u){
//   this.router.navigateByUrl("/info/" + u.id);
// }

editUser(id : number){
  this.router.navigate(['/editUser', id]);
}
}
