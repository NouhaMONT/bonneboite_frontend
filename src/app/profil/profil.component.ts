import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'src/service/authentification.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  public name ='';
  public role ='';
  public nom ='';
  public prenom ='';
  public email ='';
  
  constructor(public authService : AuthentificationService) { }

  ngOnInit(): void {
    //this.user= localStorage.getItem('authToken');
    //console.log("test profil"+this.user)
    this.name=localStorage.getItem("name");
    this.role=localStorage.getItem("role");
    this.nom=localStorage.getItem("nom");
    this.prenom=localStorage.getItem("prenom");
    this.email=localStorage.getItem("email");
  }

}
