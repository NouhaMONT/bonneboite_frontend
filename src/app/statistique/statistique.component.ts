import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { ApiService } from 'src/service/api.service';

@Component({
  selector: 'app-statistique',
  templateUrl: './statistique.component.html',
  styleUrls: ['./statistique.component.css']
})
export class StatistiqueComponent implements OnInit {

   public rome_code ;
   public nbr_companies =[];
   //public data =[]; 
   public companies; 
   public nbr;
   public barChartOptions: {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  //public barChartLabels = ['Juillet', 'Aout', 'Septembre', 'Octobre'];
  public barChartLabels = ['mois-courant -3', 'mois-courant -2', 'mois-courant -1', 'mois-courant'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [], label: 'nbr entreprises' },
    // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];

  constructor( public apiService: ApiService) { }

  ngOnInit(): void {
    this.rechercher();
  }

  rechercher(){
    this.apiService.rechercheByMetier(this.rome_code)
     .subscribe(response=>{
       this.companies= response;
       
      // console.log(this.nbr);
       console.log(this.companies.length);
       console.log(this.companies);
       console.log(this.companies[0]._id); 
      if (this.companies){
        this.nbr_companies =[];
        for(let i=0; i< this.companies.length; i++){
          this.nbr_companies[i] = this.companies[i].companies_count; 
         //console.log(this.nbr);
        //  data.push(this.nbr);
        //  console.log(data);
        }
        //data.push(this.nbr);
          console.log(this.nbr_companies);
          this.barChartData = [
            { data: this.nbr_companies, label: ' nombre des entreprises' },
            // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
          ];
      }
      
     }, 
     err =>{
      console.log(err); 
    }
    
      ) 
     
     
    
    
    
  }


}
