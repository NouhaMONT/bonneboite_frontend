import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MenuComponent } from './menu/menu.component';
import { ListUserComponent } from './list-user/list-user.component';
import { ListPreferenceComponent } from './list-preference/list-preference.component';
import { NewPreferenceComponent } from './new-preference/new-preference.component';
import { ToastrModule } from 'ngx-toastr';
import { ChartsModule} from 'ng2-charts'; 
import { InfosComponent } from './infos/infos.component';
import { EntreprisesComponent } from './entreprises/entreprises.component';
import { StatistiqueComponent } from './statistique/statistique.component';
import { ProfilComponent } from './profil/profil.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { EditUserComponent } from './edit-user/edit-user.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent, 
    MenuComponent, 
    ListUserComponent,
    ListPreferenceComponent,
    NewPreferenceComponent,
    InfosComponent,
    EntreprisesComponent,
    StatistiqueComponent,
    ProfilComponent,
    HomeComponent,
    ContactComponent,
    EditUserComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule, 
    AppRoutingModule, 
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ChartsModule,
    
   
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
