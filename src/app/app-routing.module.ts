import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { EntreprisesComponent } from './entreprises/entreprises.component';
import { HomeComponent } from './home/home.component';
import { InfosComponent } from './infos/infos.component';
import { ListPreferenceComponent } from './list-preference/list-preference.component';
import { ListUserComponent } from './list-user/list-user.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { NewPreferenceComponent } from './new-preference/new-preference.component';
import { ProfilComponent } from './profil/profil.component';
import { RegisterComponent } from './register/register.component';
import { StatistiqueComponent } from './statistique/statistique.component';

const routes: Routes = [
  {path: '', component:MenuComponent, children : [
    {path:'list-user', component: ListUserComponent},
    {path:'preferences', component: ListPreferenceComponent},
    {path:'new-pref', component: NewPreferenceComponent},
    {path:'infoUser/:id', component: InfosComponent},
    {path:'entreprises', component: EntreprisesComponent},
    {path:'statistiques', component:StatistiqueComponent},
    {path:'profil', component:ProfilComponent},
    {path:'home', component:HomeComponent},
    {path:'contact', component:ContactComponent},
    {path:'editUser/:id', component:EditUserComponent}
    
  ]},
  {path: 'login', component:LoginComponent},
  {path: 'register', component:RegisterComponent},
  
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
