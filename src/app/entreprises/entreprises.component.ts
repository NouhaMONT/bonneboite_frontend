import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/service/api.service';





@Component({
  selector: 'app-entreprises',
  templateUrl: './entreprises.component.html',
  styleUrls: ['./entreprises.component.css']
})
export class EntreprisesComponent implements OnInit {

  public entreprises; 
  public companies; 
  matched_rome_label: string; 
  city :string; 
  constructor(public apiService: ApiService) { }

  ngOnInit(): void {
    // this.getAllcompanies(); 
  }

  private getAllcompanies(){
   this.apiService.getRessource("/allcompanies")
   .subscribe(data=>{
      this.entreprises = data; 
      console.log(this.entreprises);
   }, err =>{
     console.log(err); 
   }
    )

  }
   
  rechercher(){
    this.apiService.rechercheEntreprisesMV(this.matched_rome_label, this.city)
     .subscribe(response=>{
       this.companies= response;
       console.log(this.companies);
     }, 
     err =>{
      console.log(err); 
    }
      ) 
    
  }

  // download(){
  //   var element = document.getElementById('listEnreprises')

  //   html2canvas(element).then((canvas) =>{

  //     console.log(canvas)
    
  //     var imgData = canvas.toDataURL('image/png');
  //     var doc = new jspdf.jsPDF()
  //     doc.addImage(imgData,0,0, 800, 8500);
  //     doc.save("listeEntreprises.pdf")
      

  //   }
   

    // )
  // }
 
   }


