import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthentificationService } from 'src/service/authentification.service';
import { UserService } from 'src/service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: any={};
  email: string; 
  password: string;
  username: string; 
  errorMessage: string;
  name: string;
  firstname: string; 
  role: string; 
  public token;
  public isAuthenticated:boolean; 
  public userAuthenticated; 
  loginForm: FormGroup; 

  constructor(private router: Router, 
    public userService: UserService,
    public fb: FormBuilder,
     public toastr: ToastrService) { console.log("user"+this.user);}

  ngOnInit() {
    this.userService.islogin = false;
    this.userService.admin = false;
    this.userService.suser = false;
    this.loginForm= this.fb.group({
      'username' : [null, Validators.required], 
      'password' : [null, Validators.required]
    })
  }

  

  onlogin(){
    const val = this.loginForm.value; 
    this.userService.login(val.username, val.password).subscribe(
      res=>{
        this.user = res;
        console.log("username :"+this.user.username + "role :"+this.user.role +"nom :"+ this.user.nom +"prenom :"+ this.user.prenom);
        localStorage.setItem("name", this.user.username);
        localStorage.setItem("role", this.user.role);
        localStorage.setItem("nom", this.user.nom);
        localStorage.setItem("prenom", this.user.prenom);
        localStorage.setItem("email", this.user.email);

        this.token= {name: this.user.nom, role: this.user.role, firstname: this.user.prenom, email: this.user.email, username: this.user.username};
        localStorage.setItem('authToken', this.token);
        // alert(this.user.jwt);
        // alert(this.user.username);
        let jwt ="Bearer "+ this.user.jwt;
        localStorage.setItem("token", jwt)

        this.userService.islogin= true; 
        if (this.user)
        {
          this.isAuthenticated=true;
          this.userAuthenticated=this.user;
          // this.saveAuthentificatedUser();
          this.router.navigateByUrl('/home');
        }
        else 
        {
          this.isAuthenticated=false;
          this.userAuthenticated= undefined;
        } 
        this.router.navigateByUrl('/home');
        }, error =>
        this.toastr.warning('Login Incorrecte')

      
    );
  }




  // onlogin(){
  //   this.userService.login(this.email)
  //   .subscribe(response =>{
  //     this.user = response; 
  //     if (this.user.motDepass == this.password)
  //     {
  //       this.name= this.user.nom;
  //       this.token= {name: this.user.nom, role: this.user.role, firstname: this.user.prenom, email: this.user.email};
  //       console.log(this.token);
  //       // localStorage.setItem('name', this.name);
  //       this.userService.islogin= true; 
  //       if (this.user)
  //       {
  //         this.isAuthenticated=true;
  //         this.userAuthenticated=this.user;
  //         this.saveAuthentificatedUser();
  //         this.router.navigateByUrl('/home');
  //       }
  //       else{
  //         this.isAuthenticated=false;
  //         this.userAuthenticated= undefined;
  //       }
  //     }
  //     else
  //     {
  //       this.toastr.warning( 'Mot de Passe  Incorrecte ')
  //     }
  //   },
  //   error=>
  //   this.toastr.warning( 'Login Incorrecte ')
  //   );
  // }

  // public isAdmin(){
  //   if (this.user){
  //     if (this.user.role.indexOf('ADMIN')>-1)
  //     return true;
  //   }
  //   return false;
  // }

//   public saveAuthentificatedUser(){
//     if(this.userAuthenticated){
//       localStorage.setItem('authToken', btoa(JSON.stringify(this.token)));
//     }
//   }
// }

  // public loadAuthenticatedUserFromLStg(){
  //   let t = localStorage.getItem('authToken');
  //   if (t){
  //     let user =JSON.parse(atob(t));
  //     console.log(user);
  //     this.userAuthenticated={nom:user.nom, role:user.role}
  //     console.log(this.userAuthenticated);
  //     this.isAuthenticated=true; 
  //     this.token=t;
  //   }
  // }

  //   public removeTokenLS(){
  //     localStorage.removeItem('authToken');
  //     this.isAuthenticated=false;
  //     this.token=undefined;
  //     this.userAuthenticated=undefined;
  //   }

  /** */

  // login(){
  //   this.userService.login(this.email)
  //   .subscribe(response =>{
  //     this.user = response; 
  //     if (this.user.motDepass == this.password)
  //     {
  //       this.name= this.user.nom;
  //       localStorage.setItem('name', this.name);
  //       this.userService.islogin= true; 
  //       if (this.user.role == "ADMIN")
  //       {
  //         this.userService.admin = true; 
  //         this.router.navigateByUrl('/');
  //       }
  //       else{
  //         this.userService.suser= true; 
  //         this.router.navigateByUrl('/');
  //       }
  //     }
  //     else
  //     {
  //       this.toastr.warning( 'Mot de Passe  Incorrecte ')
  //     }
  //   },
  //   error=>
  //   this.toastr.warning( 'Login Incorrecte ')
  //   );
  // }


}
