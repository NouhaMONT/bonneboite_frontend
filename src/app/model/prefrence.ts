export class Preference{
    id: number; 
    ville_metier: string; 
    code_rome: string; 
    longitude: number; 
    latitude: number;
}