export class Entreprise {
    public address:String; 
    public alternance:Boolean; 
    public boosted:Boolean ;
    public city:String; 
    public contact_mode : String ; 
    public distance: Number;
    public headcount_text:String;
    public lat:Number; 
    public lon:Number;
    public matched_rome_code:String; 
    public matched_rome_label:String; 
    public matched_rome_slug:String; 
    public naf:String; 
    public naf_text:String; 
    public name:String; 
    public siret:String; 
    public social_network:String;
    public stars: Number;
    public url : String;
    
}