import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/service/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public userService: UserService,
    public fb: FormBuilder, private router: Router,
    public toastr: ToastrService
    ) { }

  ngOnInit(): void {
    this.infoForm();
  }

  infoForm(){
    this.userService.dataForm= this.fb.group({
      id: null, 
      nom:['',[Validators.required, Validators.minLength(5)]],
      prenom:['',[Validators.required, Validators.minLength(5)]],
      username:['',[Validators.required, Validators.minLength(5)]],
      email:['',[Validators.required, Validators.minLength(8)]],
      role:['',[Validators.required, Validators.minLength(8)]],
      motDepass:['',[Validators.required, Validators.minLength(3)]],
    });
  }

  onSubmit(){
  this.addData();
  }
  
  addData(){
    this.userService.createData(this.userService.dataForm.value)
    .subscribe(data =>{
      this.toastr.success('Compte créé avec succès !');
      this.router.navigateByUrl('/login');
    })
  }

}
