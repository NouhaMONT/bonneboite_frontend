import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/service/user.service';


@Component({
  selector: 'app-infos',
  templateUrl: './infos.component.html',
  styleUrls: ['./infos.component.css']
})
export class InfosComponent implements OnInit {

  constructor(
    public route:ActivatedRoute,
    public router: Router, 
    public userService: UserService ) { }

    public user;
    id: number; 
  ngOnInit(): void {
    this.id=+this.route.snapshot.params['id'];
    // this.userService.getData(this.id)
    // .subscribe(data =>
    //   this.user =data);
    //   console.log(this.user);

   
      this.userService.getRessource("/users/"+this.id)
      .subscribe(data=>{
        this.user = data; 
      },err=>{
        console.log(err);
      })
    
    
  }

  
  

}
