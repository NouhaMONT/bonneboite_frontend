import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreferenceService } from 'src/service/preference.service';
import { UserService } from 'src/service/user.service';
import { NewPreferenceComponent } from '../new-preference/new-preference.component';




@Component({
  selector: 'app-list-preference',
  templateUrl: './list-preference.component.html',
  styleUrls: ['./list-preference.component.css']
})
export class ListPreferenceComponent implements OnInit {
public preferences;
public role='';


  constructor(private userService: UserService, 
   public prefService: PreferenceService, public toastr: ToastrService,
   private router: Router) { }

  ngOnInit(): void {
    this.getData();
    this.role=localStorage.getItem("role");
  }

  // private getPreferences(){
  //   this.userService.getRessource("/prefrences")
  //   .subscribe(data=>{
  //     this.preferences=data;
  //   },err=>{
  //    console.log(err);
     
  //   })
  // }

   getData(){
    this.prefService.getAll().subscribe(
      response =>{this.prefService.listData = response; 
        console.log("test"+response)
      }
      
    ); 
  }

  removeData(id: number){
    if (window.confirm('Vous êtes sûr ! Vous voulez supprimez cette préférence? ')){
        this.prefService.deleteData(id)
        .subscribe(
          data=>{
            console.log(data);
            this.toastr.warning('préférence supprimée!');
            this.getData();
            
          },
          error => console.log(error));
    }
  }

isAdmin(){
  if (this.role=="ADMIN"){
    return true; 
  }else {return false ;}
}

  

}
