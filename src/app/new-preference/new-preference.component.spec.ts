import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPreferenceComponent } from './new-preference.component';

describe('NewPreferenceComponent', () => {
  let component: NewPreferenceComponent;
  let fixture: ComponentFixture<NewPreferenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewPreferenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPreferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
