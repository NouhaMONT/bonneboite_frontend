import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogModule, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PreferenceService } from 'src/service/preference.service';
import {FormControl, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-new-preference',
  templateUrl: './new-preference.component.html',
  styleUrls: ['./new-preference.component.css']
})
export class NewPreferenceComponent implements OnInit {

  ville_metier: string; 
    code_rome: string; 
    longitude: number; 
    latitude: number;
  constructor(public prefService: PreferenceService, 
    public fb: FormBuilder,public toastr: ToastrService,
    private router: Router,
    ) { }

  ngOnInit(): void {
    this.infoForm();
  }

  infoForm(){
    this.prefService.dataForm= this.fb.group(
      {
        id: null,
        ville_metier:['',[Validators.required]],
        code_rome:['',[Validators.required]],
        longitude:['',[Validators.required]],
        latitude:['',[Validators.required]],
     
      });
  }

  ResetForm(){
    this.prefService.dataForm.reset();
  }

  onSubmit(){
    this.addData();
  }
  
  addData(){
    const val = this.prefService.dataForm.value; 
    this.prefService.createData(this.prefService.dataForm.value)
    .subscribe(data => {

      this.toastr.success('Ajout effectué avec succès !');

      this.prefService.getAll().subscribe(
        response =>{this.prefService.listData = response; }
      );
      
      this.router.navigateByUrl('/preferences');
    })
  }
}
