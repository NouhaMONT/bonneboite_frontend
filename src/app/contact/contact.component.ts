import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  
  constructor(
    public toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    
  }

  onSubmit(){
    this.toastr.success('message envoyé');
    this.router.navigateByUrl('home');
  }
   
  
 

}




